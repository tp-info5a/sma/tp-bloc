import models.Agent;
import models.Environnement;

public class Main {
    public static void main(String[] args) {
        Environnement env = new Environnement();
        env.initBloc();

        int iteration = 0;

        System.out.println("Init : " + env.toString());

        Agent agent;
        do {
            /* méthodes
                0 : random
                1 : un agent apres l'autre
                2 : un agent avec le goal le plus bas en 1er
             */
            agent = env.choseAgent(2, iteration);

            agent.percevoir(env);
            agent.action(env);

            iteration += 1;
            System.out.println(iteration + ": " + env.toString());
        } while (!env.isAllAgentSatisfied() && iteration < 1000);

        System.out.println("Fin : " + env.isAllAgentSatisfied());
    }
}
