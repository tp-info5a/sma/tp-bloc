package models;

import java.util.ArrayList;
import java.util.List;

public class Case {
    private final List<Agent> agentList;

    public Case() {
        this.agentList = new ArrayList<>();
    }

    public List<Agent> getAgentList() {
        return agentList;
    }

    @Override
    public String toString() {
        return "Case" + agentList;
    }
}
