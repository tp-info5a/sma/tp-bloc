package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class Environnement {
    private final List<Agent> agentList;
    private final List<Case> caseList;
    private Agent lastAgentSelected;

    public Environnement() {
        this.agentList = new ArrayList<>();
        this.caseList = new ArrayList<>();
        this.lastAgentSelected = new Agent("null","null");
    }

    public void initBloc(){
        this.AddNewCase();
        this.AddNewCase();
        this.AddNewCase();

        this.addAgent(new Agent("A", "B"), 0);
        this.addAgent(new Agent("B", "Table"), 0);
        this.addAgent(new Agent("C", "A"), 0);
        this.addAgent(new Agent("D", "C"), 0);
    }

    public void addAgent(Agent agent, Integer index) {
        this.agentList.add(agent);
        this.caseList.get(index).getAgentList().add(agent);
    }

    public void AddNewCase() {
        this.caseList.add(new Case());
    }

    public void move(Agent agent) {
        Random random = new Random();

        Optional<Case> optionalCase = this.caseList.stream()
                .filter(c -> c.getAgentList().contains(agent))
                .findFirst();

        List<Case> collect = this.caseList.stream()
                .filter(c -> !c.getAgentList().contains(agent))
                .collect(Collectors.toList());

        Optional<Agent> goal = this.agentList.stream()
                .filter(agent1 -> agent1.getName().equals(agent.getGoal()))
                .findFirst();

        Optional<Case> optionalCase1 = Optional.empty();

        if (!agent.getGoal().equals("Table")){
            optionalCase1 = this.caseList.stream()
                    .filter(c -> !c.getAgentList().contains(agent))
                    .filter(c -> goal.map(value -> c.getAgentList().contains(value)).orElse(Boolean.FALSE))
                    .findFirst();
        }

        Case aCase = optionalCase1.orElseGet(() -> collect.get(random.nextInt(collect.size())));

        optionalCase.ifPresent(c -> c.getAgentList().remove(agent));

        aCase.getAgentList().add(agent);

    }

    public String precevoirBelow(Agent agent) {
        Optional<Case> optionalCase = this.caseList.stream().filter(c -> c.getAgentList().contains(agent)).findFirst();

        if (optionalCase.isPresent()) {
            Case aCase = optionalCase.get();
            int index = aCase.getAgentList().indexOf(agent);
            if (index == 0) {
                return "Table";
            }
            Agent agent1 = aCase.getAgentList().get(index - 1);

            return agent1.getName();
        }

        return "erreur";
    }


    public Boolean percevoirIsFree(Agent agent) {
        Optional<Case> caseOptional = this.caseList.stream().filter(c -> c.getAgentList().contains(agent)).findFirst();

        if (caseOptional.isPresent()) {
            Case aCase = caseOptional.get();

            int i = aCase.getAgentList().indexOf(agent);

            return i == (aCase.getAgentList().size() - 1) ;
        }

        return Boolean.FALSE;
    }

    public Boolean percevoirIsPushed(Agent agent) {
        Optional<Case> caseOptional = this.caseList.stream().filter(c -> c.getAgentList().contains(agent)).findFirst();

        if (caseOptional.isPresent()) {
            Case aCase = caseOptional.get();

            int i = aCase.getAgentList().indexOf(agent);

            return  i == 0 ? Boolean.FALSE : aCase.getAgentList().get(i - 1).getPushing();
        }

        return Boolean.FALSE;
    }

    public Boolean isAllAgentSatisfied() {
        Optional<Agent> agentOptional = this.agentList.stream()
                .filter(agent -> !agent.getSatisfied())
                .findAny();

        return agentOptional.isEmpty();
    }

    public Agent choseAgent(Integer methode, Integer iteration) {
        switch(methode){
            case 0:
                Random random = new Random();
                return this.agentList.get(random.nextInt(this.agentList.size()));

            case 1:
                return this.agentList.get(iteration % this.agentList.size());
            case 2:
                Agent agent;
                if(lastAgentSelected.getName().equals("null")){
                    agent = getAgentWithGoal("Table");
                }
                else {
                    agent = getAgentWithGoal(lastAgentSelected.getName());
                }

                this.lastAgentSelected = agent;
                return agent;
            default:
                System.out.println("unknown choice method");
                return this.agentList.get(iteration % this.agentList.size());
        }
    }

    public Agent getAgentWithGoal(String goal){
        Agent table = new Agent("temp","temp");
        for(int i = 0; i < this.agentList.size();i++){
            Agent temp = agentList.get(i);
            if(temp.getGoal().equals("Table")){
                table = temp;
            }
            if(temp.getGoal().equals(goal)){
                return temp;
            }
        }
        return table;
    }
    @Override
    public String toString() {
        return this.caseList.toString();
    }
}
