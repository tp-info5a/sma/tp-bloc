package models;

public class Agent {
    private String name;
    private String goal;
    private Boolean isFree;
    private Boolean isPushed;
    private Boolean isPushing;
    private String below;
    private Boolean isSatisfied;

    public Agent(String _n, String _g){
        this.name = _n;
        this.goal = _g;
        this.isSatisfied = Boolean.FALSE;
        this.isPushed = Boolean.FALSE;
        this.isFree = Boolean.FALSE;
        this.isPushing = Boolean.FALSE;
    }

    public void percevoir(Environnement env){
        this.below = env.precevoirBelow(this);
        this.isFree = env.percevoirIsFree(this);
        this.isPushed = env.percevoirIsPushed(this);
    }

    public void action(Environnement env){
        this.isPushing = false;
        isSatisfied = below.equals(goal) && !this.isPushed;
        if(!this.isSatisfied){
            if(this.isFree){
                env.move(this);
            }
            else{
                this.isPushing = true;
            }
        }
    }

    public String getName(){
        return name;
    }

    public Boolean getFree() {
        return isFree;
    }

    public Boolean getPushed() {
        return isPushed;
    }

    public Boolean getSatisfied() {
        return isSatisfied;
    }

    public String getBelow() {
        return below;
    }

    public String getGoal() {
        return goal;
    }

    public Boolean getPushing() {
        return isPushing;
    }

    public void setPushing(Boolean pushing) {
        isPushing = pushing;
    }

    public void setBelow(String below) {
        this.below = below;
    }

    public void setFree(Boolean free) {
        isFree = free;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }

    public void setSatisfied(Boolean satisfied) {
        isSatisfied = satisfied;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
